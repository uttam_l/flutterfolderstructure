import 'dart:async';
// import 'dart:convert';
// import 'dart:io';

import 'package:folder_structure_example/models/userModel.dart';
import 'package:folder_structure_example/utils/databaseHelper.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:folder_structure_example/pages/acceptedList.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:rxdart/rxdart.dart';
import 'package:logger/logger.dart';

class AccepedListBloc {
  List<UserModel> acceptedList = [];

  DatabaseHelper databaseHelper = new DatabaseHelper();
  int count = 0;

  final _listController = StreamController<List<UserModel>>.broadcast();
  final _acceptedListofUsers = BehaviorSubject<List<UserModel>>();

  final _accepted = StreamController<UserModel>();
  final _rejected = StreamController<UserModel>();
  final _acceptedUser = StreamController<UserModel>();

  Stream<List<UserModel>> get acceptedListStream => _listController.stream;
  StreamSink<List<UserModel>> get acceptedListSink => _listController.sink;

  Stream<List<UserModel>> get getAcceptedListStream =>
      _acceptedListofUsers.stream;
  StreamSink<List<UserModel>> get getAcceptedListSink =>
      _acceptedListofUsers.sink;

  Stream<UserModel> get acceptedUserModelStream => _accepted.stream;
  StreamSink<UserModel> get acceptedUserModelSink => _accepted.sink;

  Stream<UserModel> get rejectedUserModelStream => _rejected.stream;
  StreamSink<UserModel> get rejectedUserModelSink => _rejected.sink;

  Stream<UserModel> get acceptedUserListModelStream => _acceptedUser.stream;
  StreamSink<UserModel> get acceptedUserListModelSink => _acceptedUser.sink;

  AccepedListBloc() {
    _listController.add(acceptedList);
    _acceptedListofUsers.add(acceptedList);
    _accepted.stream.listen(_acceptedList);
    _rejected.stream.listen(_rejectedList);
    _acceptedListofUsers.stream.listen(_getUsersFromDb);
  }

  void dispose() {
    _listController.close();
    _accepted.close();
    _rejected.close();
    _acceptedListofUsers.close();
    _acceptedUser.close();
  }

  var logger = new Logger();

  void _acceptedList(UserModel user) {
    var isAlredyUser = false;
    if (acceptedList != null) {
      for (var usr in acceptedList) {
        logger.e("User here is :  " + usr.name);
        if (usr.id == user.id) {
          isAlredyUser = true;
        }
      }
    }
    if (!isAlredyUser) {
      acceptedList.add(user);
      _insertUsers(user);
      getAcceptedListSink.add(acceptedList);
      // _acceptedListofUsers.add(acceptedList);
    }
  }

  void _rejectedList(UserModel user) async {
    var result = await databaseHelper.deleteFromDb(user.id);
    for (var usr in acceptedList) {
      if (usr.id == user.id) {
        acceptedList.removeWhere((user) => user.id == usr.id);

        if (result != 0) {
          print("Success");
        } else {
          logger.e("error deletinggg");
        }
      }
    }
  }

  void _insertUsers(UserModel userModel) async {
    // if(acceptedList == null){
    //   acceptedList = List<UserModel>();
    // }
    var result = await databaseHelper.insertUserstoDb(userModel);
    if (result != 0) {
      logger.i("**************Success*****************");
    }
  }

  Future<List<UserModel>> _fetchUser() async {
    List<UserModel> result = await databaseHelper.getUserListFromDb();
    // logger.i(result);
    return result;
  }

  void _getUsersFromDb(List<UserModel> event) async{
    var result = await _fetchUser();
    _acceptedListofUsers.sink.add(result);
  }
}
