import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:folder_structure_example/models/userModel.dart';
import 'package:folder_structure_example/pages/acceptedList.dart';
// import 'package:folder_structure_example/pages/acceptedList.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DatabaseHelper {
  static DatabaseHelper _databaseHeler;
  static Database _database;

  String userTabel = 'userTable';
  String userid = 'id';
  String username = 'name';
  String useravatar = 'avatar';

  DatabaseHelper._createInstance();

  factory DatabaseHelper(){
    if(_databaseHeler == null){
      _databaseHeler = DatabaseHelper._createInstance();
    }
    return _databaseHeler;
  }

  Future<Database> get database async{
    if(_database == null){
      _database = await initializeDatabase();
    }
    return _database;
  }

  Future<Database> initializeDatabase() async{
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'users.db';
    var userBatabase = await openDatabase(path, version: 1, onCreate: _createDb);
    return userBatabase;
  }

  void _createDb(Database db, int version) async{
    await db.execute('CREATE TABLE $userTabel($userid INTEGER PRIMARY KEY, $username TEXT, $useravatar TEXT)');
  }

  // CRUD

  Future<List<Map<String, dynamic>>> getUsersFromDb() async{
    Database db = await this.database;
    var result = await db.rawQuery('SELECT * FROM $userTabel');
    return result;
  }

  Future<int> insertUserstoDb(UserModel userModel) async{
    Database db = await this.database;
    // logger.e(userModel.toMap());
    var result = await db.insert(userTabel, userModel.toMap());
    return result;
  }

  Future<int> deleteFromDb(int id)async{
    var db = await this.database;
    logger.i('>>>>>>>>>>>>>>>>>>>>> $id <<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    int result = await db.rawDelete('DELETE FROM $userTabel WHERE $userid = $id');
    return result;
  }
  // TODO: update operations.

  Future<int> getCount() async{
    Database db = await this.database;
    List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) FROM $userTabel');
    int result = Sqflite.firstIntValue(x);
    return result;
  }
  
  Future<List<UserModel>> getUserListFromDb() async{
    var userMapList = await getUsersFromDb();
    List<UserModel> userList = List<UserModel>();
    int count = userMapList.length;

    for(int i = 0; i < count; i++){
      userList.add(UserModel.fromMapObject(userMapList[i]));
    }
    return userList;
  }
}